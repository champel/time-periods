const path = require('path');

module.exports = {
  mode: "development",
  devtool: "source-map",
  entry: './src/index.js',
  devServer: {
    contentBase: './static',
    compress: true,
    port: 9000
  },
  output: {
    filename: 'time-slot-chart.js',
    library: 'timeSlotChart',
    libraryExport: 'default',
    libraryTarget: 'window',
    path: path.resolve(__dirname, 'dist')
  },
  module: {
    rules: [
      {
        test: /\.coffee$/,
        use: [ 'coffee-loader' ]
      }
    ]
  },
  resolve: {
      extensions: [ '.coffee', '.js' ]
  },
  externals: {
    d3: 'd3'
  }
};
