## Time periods

Show different states on time periods

![Example](example.png)

## Scripts

1. `start`: Starts the development server
2. `test`: Executes the unit tests
3. `test:watch`: Executes the unit tests in watch mode
4. `it:open`: Opens the integration tests console, requires the development server running
5. `build`: Generates the module and the map file in ./dist
