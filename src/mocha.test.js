// Configure Chai Jest Diff
const chai = require('chai')
const chaiJestDiff = require('chai-jest-diff')
chai.use(chaiJestDiff.default())

// Configure Chai Jest Snapshots
const chaiJestSnapshot = require('chai-jest-snapshot');
chai.use(chaiJestSnapshot);
before(function() {
  chaiJestSnapshot.resetSnapshotRegistry();
});
beforeEach(function() {
  chaiJestSnapshot.configureUsingMochaContext(this);
});