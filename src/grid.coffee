export default grid = (selection, yScale, xScale, dataset) ->
  # create vertical grid
  selection.selectAll 'line.vert_grid'
    .data xScale.ticks()
    .enter()
    .append 'line'
    .attr 'class', 'vert_grid'
    .attr 'x1', (d, i) -> xScale d
    .attr 'x2', (d, i) -> xScale d
    .attr 'y1', 0
    .attr 'y2', yScale.range()[1]
    .style 'stroke', '#dddddd'
    .style 'stroke-width', '0.5px'

  selection.selectAll 'line.horz_grid'
    .data dataset
    .enter()
    .append 'line'
    .attr 'class', 'horz_grid'
    .attr 'x1', 0
    .attr 'x2', xScale.range()[1]
    .attr 'y1', (d, i) -> (yScale i) + yScale.bandwidth() / 2
    .attr 'y2', (d, i) -> (yScale i) + yScale.bandwidth() / 2
    .style 'stroke', '#dddddd'
    .style 'stroke-width', '0.5px'
