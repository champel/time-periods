export default legend = (selection) ->
  selection.append 'rect'
      .attr 'x', 0
      .attr 'y', 0
      .attr 'height', 15
      .attr 'width', 15
      .attr 'class', 'rect_has_data'

  selection.append 'text'
      .attr 'x',  20
      .attr 'y', 8.5
      .text 'Data available'
      .attr 'class', 'legend'

  selection.append 'rect'
      .attr 'x', 0
      .attr 'y', 17
      .attr 'height', 15
      .attr 'width', 15
      .attr 'class', 'rect_has_no_data'

  selection.append 'text'
      .attr 'x', 20
      .attr 'y', 8.5 + 15 + 2
      .text 'No data available'
      .attr 'class', 'legend'
