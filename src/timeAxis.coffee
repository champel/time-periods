import * as d3 from "d3"

export default timeAxis = (selection, xScale, emphasizeYearTicks) ->
  axis = d3
    .axisTop(xScale)

  selection.call axis
  
  # rework ticks and grid for better visual structure
  isYear = (t) -> +t is +(new Date(t.getFullYear(), 0, 1, 0, 0, 0))
  isMonth = (t) -> +t is +(new Date(t.getFullYear(), t.getMonth(), 1, 0, 0, 0))

  xTicks = xScale.ticks()
  isYearTick = xTicks.map(isYear)
  isMonthTick = xTicks.map(isMonth)

  # year emphasis
  # ensure year emphasis is only active if years are the biggest clustering unit
  if emphasizeYearTicks and not (isYearTick.every((d) -> d is true)) and isMonthTick.every((d)-> d is true)
    d3.selectAll 'g.tick'
      .each (d, i) -> if isYearTick[i]
        d3.select(this)
            .attr 'class': 'x_tick_emph'
    d3.selectAll '.vert_grid'
      .each (d, i) -> if (isYearTick[i])
        d3.select(this)
            .attr 'class': 'vert_grid_emph'
