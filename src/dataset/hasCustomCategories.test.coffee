import { expect } from 'chai'
import hasCustomCategories from './hasCustomCategories'

describe 'hasCustomCategories', () ->
  it 'has custom categories if the first data element category is not 0 or one ', () ->
    dataset = [
        "data": [
            ["2016-01-01 12:00:00", "Kim"],
        ]
    ]
    expect(hasCustomCategories dataset).to.equal true

  it 'does not have custom categories if the first data element category is 0 or one ', () ->
    dataset = [
        "data": [
            ["2016-01-01 12:00:00", 0],
        ]
      ,
        "data": [
            ["2016-01-01 12:00:00", 1],
        ]
    ]
    expect(hasCustomCategories dataset).to.equal false
