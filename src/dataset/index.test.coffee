import { expect } from 'chai'
import { hasDefinedBlocks, hasCustomCategories, normalize, cluster, range } from '.'

initDateTime = new Date "2016-01-01T11:00:00.000Z"
endDateTime = new Date "2016-01-01T12:00:00.000Z"

describe 'integration test', () ->
  it 'should prepare a data structure from two blocks with date times in text and custom categories', () ->
    dataset = [
        "measure": "Fat Bike",
        "categories":
            "Kim": { "color": "#377eb8" }
            "Bert": { "color": "#ff7f00" }
            "Zoe": { "color": "purple" }
        "data": [
            ["2016-01-01 12:00:00", "Kim", "2016-01-01 13:00:00"]
            ["2016-01-01 14:22:51", "Zoe", "2016-01-01 16:14:12"]
            ["2016-01-01 16:14:12", "Bert", "2016-01-01 17:14:12"]
            ["2016-01-01 19:20:05", "Zoe", "2016-01-01 20:30:00"]
            ["2016-01-01 20:30:00", "Kim", "2016-01-01 22:00:00"]
        ]
      ,
        "measure": "Fat Bike",
        "categories":
            "Kim": { "color": "#377eb8" }
            "Bert": { "color": "#ff7f00" }
            "Zoe": { "color": "purple" }
        "data": [
            ["2016-01-01 12:00:00", "Kim", "2016-01-01 13:00:00"]
            ["2016-01-01 14:22:51", "Zoe", "2016-01-01 16:14:12"]
            ["2016-01-01 16:14:12", "Bert", "2016-01-01 17:14:12"]
            ["2016-01-01 19:20:05", "Zoe", "2016-01-01 20:30:00"]
            ["2016-01-01 20:30:00", "Kim", "2016-01-01 22:00:00"]
        ]
    ]

    definedBlocks = hasDefinedBlocks dataset
    customCategories = hasCustomCategories dataset

    normalize dataset, definedBlocks
    cluster dataset, definedBlocks
    [ startDate, endDate ] = range dataset, [ 0, 0 ]

    expect(definedBlocks).to.equal true
    expect(customCategories).to.equal true
    expect(dataset).to.deep.equal [
        "measure":"Fat Bike",
        "categories":
          "Kim": {"color":"#377eb8"}
          "Bert": {"color":"#ff7f00"}
          "Zoe": {"color":"purple"}
        "data": [
          [new Date("2016-01-01T11:00:00.000Z"),"Kim", new Date("2016-01-01T12:00:00.000Z"),0]
          [new Date("2016-01-01T13:22:51.000Z"),"Zoe", new Date("2016-01-01T15:14:12.000Z"),0]
          [new Date("2016-01-01T15:14:12.000Z"),"Bert",new Date("2016-01-01T16:14:12.000Z"),0]
          [new Date("2016-01-01T18:20:05.000Z"),"Zoe", new Date("2016-01-01T19:30:00.000Z"),0]
          [new Date("2016-01-01T19:30:00.000Z"),"Kim", new Date("2016-01-01T21:00:00.000Z"),0]
        ]
        "disp_data": [
            [new Date("2016-01-01T11:00:00.000Z"),"Kim", new Date("2016-01-01T12:00:00.000Z"),0]
            [new Date("2016-01-01T13:22:51.000Z"),"Zoe", new Date("2016-01-01T15:14:12.000Z"),0]
            [new Date("2016-01-01T15:14:12.000Z"),"Bert",new Date("2016-01-01T16:14:12.000Z"),0]
            [new Date("2016-01-01T18:20:05.000Z"),"Zoe", new Date("2016-01-01T19:30:00.000Z"),0]
            [new Date("2016-01-01T19:30:00.000Z"),"Kim", new Date("2016-01-01T21:00:00.000Z"),0]
          ]
      ,
        "measure":"Fat Bike",
        "categories":
          "Kim": {"color":"#377eb8"}
          "Bert": {"color":"#ff7f00"}
          "Zoe": {"color":"purple"}
        "data": [
          [new Date("2016-01-01T11:00:00.000Z"),"Kim", new Date("2016-01-01T12:00:00.000Z"),0]
          [new Date("2016-01-01T13:22:51.000Z"),"Zoe", new Date("2016-01-01T15:14:12.000Z"),0]
          [new Date("2016-01-01T15:14:12.000Z"),"Bert",new Date("2016-01-01T16:14:12.000Z"),0]
          [new Date("2016-01-01T18:20:05.000Z"),"Zoe", new Date("2016-01-01T19:30:00.000Z"),0]
          [new Date("2016-01-01T19:30:00.000Z"),"Kim", new Date("2016-01-01T21:00:00.000Z"),0]
        ],
        "disp_data": [
          [new Date("2016-01-01T11:00:00.000Z"),"Kim", new Date("2016-01-01T12:00:00.000Z"),0]
          [new Date("2016-01-01T13:22:51.000Z"),"Zoe", new Date("2016-01-01T15:14:12.000Z"),0]
          [new Date("2016-01-01T15:14:12.000Z"),"Bert",new Date("2016-01-01T16:14:12.000Z"),0]
          [new Date("2016-01-01T18:20:05.000Z"),"Zoe", new Date("2016-01-01T19:30:00.000Z"),0]
          [new Date("2016-01-01T19:30:00.000Z"),"Kim", new Date("2016-01-01T21:00:00.000Z"),0]
        ]
      ]
