export default range = (normalizedAndClusteredDataset, [ startDate, endDate ]) ->
  # determine start and end dates among all nested datasets
  newStartDate = 0
  newEndDate = 0
  normalizedAndClusteredDataset.forEach (series, seriesI) ->
    if series.disp_data.length > 0
      firstDate = series.disp_data[0][0]
      lastDate = series.disp_data[series.disp_data.length-1][2]
      if newStartDate is 0
        newStartDate = firstDate
      if newEndDate is 0
        newEndDate = lastDate
      else
        if (startDate is 0) and (firstDate[0] < newStartDate)
          newStartDate = firstDate
        if (endDate is 0) and (lastDate > newEndDate)
          newEndDate = lastDate

  [newStartDate, newEndDate]
