export default paginate = (dataset, curDisplayFirstDataset, maxDisplayDatasets) ->
  maxPages = 0 # check which subset of datasets have to be displayed
  startSet = null
  endSet = null

  if maxDisplayDatasets != 0
    startSet = curDisplayFirstDataset
    if (curDisplayFirstDataset + maxDisplayDatasets) > dataset.length
      endSet = dataset.length
    else
      endSet = curDisplayFirstDataset + maxDisplayDatasets
    maxPages = Math.ceil(dataset.length / maxDisplayDatasets)
  else
    startSet = 0
    endSet = dataset.length

  [ startSet, endSet, maxPages]
