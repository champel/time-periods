export default (dataset) ->
  countBy = (reduction, current, index) ->
    reduction[current.length] = (reduction[current.length] ? 0) + 1
    reduction
  lengths = dataset[0].data.reduce countBy, {}
  switch
    when lengths["3"] is dataset[0].data.length then true
    when lengths["2"] is dataset[0].data.length then false
    else throw new Error 'Detected different data formats in input data. Format can either be continuous data format or time gap data format but not both.'
