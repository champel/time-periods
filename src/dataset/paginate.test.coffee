import { expect } from 'chai'
import paginate from './paginate'

describe 'paginate', () ->
  it 'if maximum to display is 0, the range will cover all elements and maxPages is defined as 0', () ->
    dataset = [ 1, 2, 3, 4 ]
    start = 0
    end = 4
    maxPages = 0
    expect(paginate dataset, undefined, 0).to.deep.equal [ start, end, maxPages ]
  it 'if maximum to display is defined, uses the current position and adds the maximum of elements returning the maximum of pages available', () ->
    dataset = [ 1, 2, 3, 4 ]
    start = 2
    end = 4
    maxPages = 2
    expect(paginate dataset, 2, 2).to.deep.equal [ start, end, maxPages ]
  it 'if maximum to display is defined, the last page contains the rest of elements returning the maximum of pages available', () ->
    dataset = [ 1, 2, 3 ]
    start = 2
    end = 3
    maxPages = 2
    expect(paginate dataset, 2, 2).to.deep.equal [ start, end, maxPages ]
