import { expect } from 'chai'
import range from './range'

initDateTime = new Date "2016-01-01T11:00:00.000Z"
endDateTime = new Date "2016-01-01T12:00:00.000Z"

describe 'range', () ->
  it 'get minimum and maximum date times as range', () ->
    normalizedDataset = [
      "disp_data": [
          [initDateTime, "Kim", endDateTime]
      ]
    ]
    expect(range normalizedDataset, [ 0, 0 ]).to.deep.equal [ initDateTime, endDateTime ]
