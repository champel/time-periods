import { expect } from 'chai'
import cluster from './cluster'

initDateTime = new Date "2016-01-01T11:00:00.000Z"
endDateTime = new Date "2016-01-01T12:00:00.000Z"

initDateTime2 = new Date "2016-01-02T11:00:00.000Z"
endDateTime2 = new Date "2016-01-02T12:00:00.000Z"

describe 'cluster', () ->
  describe 'blocks', () ->
    it 'should not cluster separate blocks', () ->
      dataset = [
          "data": [
              [initDateTime, "Kim", endDateTime]
              [initDateTime2, "Kim", endDateTime2]
          ]
      ]
      cluster dataset, true
      expect(dataset).to.deep.equal [
          "data": [
              [initDateTime, "Kim", endDateTime,0]
              [initDateTime2, "Kim", endDateTime2]
          ]
          "disp_data": [
              [initDateTime, "Kim", endDateTime,0]
              [initDateTime2, "Kim", endDateTime2]
          ]
      ]

    it 'should cluster continuous blocks', () ->
      dataset = [
          "data": [
              [initDateTime, "Kim", endDateTime]
              [endDateTime, "Kim", endDateTime2]
          ]
      ]
      cluster dataset, true
      expect(dataset).to.deep.equal [
          "data": [
              [initDateTime, "Kim", endDateTime2,1]
              [endDateTime, "Kim", endDateTime2]
          ]
          "disp_data": [
              [initDateTime, "Kim", endDateTime2,1]
          ]
      ]
    it 'should not cluster continuous blocks with different topic', () ->
      dataset = [
          "data": [
              [initDateTime, "Kim", endDateTime]
              [endDateTime, "Jon", endDateTime2]
          ]
      ]
      cluster dataset, true
      expect(dataset).to.deep.equal [
          "data": [
              [initDateTime, "Kim", endDateTime,0]
              [endDateTime, "Jon", endDateTime2,0]
          ]
          "disp_data": [
              [initDateTime, "Kim", endDateTime,0]
              [endDateTime, "Jon", endDateTime2,0]
          ]
      ]


  describe 'intervals', () ->
    it 'should cluster intervals with the next one even blocks do not mach', () ->
      dataset = [
          "data": [
              [initDateTime, "Kim", endDateTime]
              [initDateTime2, "Kim", endDateTime2]
          ]
      ]
      cluster dataset, false
      expect(dataset).to.deep.equal [
          "data": [
              [initDateTime, "Kim", endDateTime2,1]
              [initDateTime2, "Kim", endDateTime2]
          ]
          "disp_data": [
              [initDateTime, "Kim", endDateTime2,1]
          ]
      ]
    it 'should not cluster intervals with different topic but extend the end dates', () ->
      dataset = [
          "data": [
              [initDateTime, "Kim", endDateTime]
              [initDateTime2, "Jon", endDateTime2]
          ]
      ]
      cluster dataset, false
      expect(dataset).to.deep.equal [
          "data": [
              [initDateTime, "Kim", initDateTime2,0]
              [initDateTime2, "Jon", endDateTime2,0]
          ]
          "disp_data": [
              [initDateTime, "Kim", initDateTime2,0]
              [initDateTime2, "Jon", endDateTime2,0]
          ]
      ]
