export default hasCustomCategories = (dataset) ->
  # check if data has custom categories
  (dataset.find (block) -> (block.data[0][1] != 0) and (block.data[0][1] != 1))?
