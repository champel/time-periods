import { expect } from 'chai'
import normalize from './normalize'

initDateTime = new Date "2016-01-01T11:00:00.000Z"
endDateTime = new Date "2016-01-01T12:00:00.000Z"

describe 'normalize', () ->
  it 'should do nothing if dates are informed in blocks', () ->
    dataset = [
        "data": [
            [initDateTime, "Kim", endDateTime],
        ]
    ]
    normalize dataset, true
    expect(dataset).to.deep.equal [
        "data": [
            [initDateTime, "Kim", endDateTime],
        ]
    ]
  describe 'date time parsing', () ->
    stringInitDateTime = "2016-01-01 12:00:00"
    stringEndDateTime = "2016-01-01 13:00:00"

    it 'should parse from a structure based on blocks', () ->
      dataset = [
          "data": [
              [stringInitDateTime, "Kim", stringEndDateTime],
          ]
      ]
      normalize dataset, true
      expect(dataset).to.deep.equal [
          "data": [
              [initDateTime, "Kim", endDateTime],
          ]
      ]

    it 'should parse from a structure based on intervals', () ->
      dataset = [
          "data": [
              [stringInitDateTime, "Kim"],
          ]
          "interval_s": 3600
      ]
      normalize dataset, false
      expect(dataset).to.deep.equal [
          "data": [
              [initDateTime, "Kim", endDateTime],
          ]
          "interval_s": 3600
      ]

    it 'should generate end block  block in a structure based on intervals', () ->
      dataset = [
          "data": [
              [initDateTime, "Kim"],
          ]
          "interval_s": 3600
      ]
      normalize dataset, false
      expect(dataset).to.deep.equal [
          "data": [
              [initDateTime, "Kim", endDateTime],
          ]
          "interval_s": 3600
      ]

  describe 'date parsing', () ->
    stringInitDate = "2016-01-01"
    stringEndDate = "2016-01-02"

    initDate = new Date "2015-12-31T23:00:00.000Z"
    endDate = new Date "2016-01-01T23:00:00.000Z"

    it 'should parse from a structure based on blocks', () ->
      dataset = [
          "data": [
              [stringInitDate, "Kim", stringEndDate],
          ]
      ]
      normalize dataset, true
      expect(dataset).to.deep.equal [
          "data": [
              [initDate, "Kim", endDate],
          ]
      ]

    it 'should parse from a structure based on intervals', () ->
      dataset = [
          "data": [
              [stringInitDate, "Kim"],
          ]
          "interval_s": 3600 * 24
      ]
      normalize dataset, false
      expect(dataset).to.deep.equal [
          "data": [
              [initDate, "Kim", endDate],
          ]
          "interval_s": 3600 * 24
      ]

    it 'should generate end block in a structure based on intervals', () ->
      dataset = [
          "data": [
              [initDate, "Kim"],
          ]
          "interval_s": 3600 * 24
      ]
      normalize dataset, false
      expect(dataset).to.deep.equal [
          "data": [
              [initDate, "Kim", endDate],
          ]
          "interval_s": 3600 * 24
      ]
