import { expect } from 'chai'
import hasDefinedBlocks from './hasDefinedBlocks'

describe 'hasDefinedBlocks', () ->
  it 'has defined blocks when the data of first dataset has three elements', () ->
    dataset = [
        "data": [
            ["2016-01-01 12:00:00", "Kim", "2016-01-01 13:00:00"],
        ]
    ]
    expect(hasDefinedBlocks dataset).to.equal true

  it 'not has defined blocks when the data of first dataset has two elements', () ->
    dataset = [
        "data": [
            ["2016-01-01 12:00:00", "Kim"],
        ]
    ]
    expect(hasDefinedBlocks dataset).to.equal false

  it 'different element counts throws an error', () ->
    dataset = [
        "data": [
            ["2016-01-01 12:00:00", "Kim"]
            ["2016-01-01 12:00:00", "Kim", "2016-01-01 13:00:00"]
        ]
    ]
    expect(-> hasDefinedBlocks dataset).to.throw()
