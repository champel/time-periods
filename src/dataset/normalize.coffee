import * as d3 from "d3"

# parse data text strings to JavaScript date stamps
dateFormat = d3.timeFormat '%Y-%m-%d'
dateTimeFormat = d3.timeFormat '%Y-%m-%d %H:%M:%S'
parseDate = d3.timeParse dateFormat
parseDateTime = d3.timeParse dateTimeFormat

parseDateRegEx = new RegExp(/^\d{4}-\d{2}-\d{2}$/)
parseDateTimeRegEx = new RegExp(/^\d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2}$/)

export default normalize = (dataset, definedBlocks) ->
  isDateOnlyFormat = true

  dataset.forEach (d) ->
    d.data.forEach (d1) ->
      if !(d1[0] instanceof Date)
        if parseDateRegEx.test d1[0]
          # d1[0] is date without time data
          d1[0] = parseDate d1[0]
        else
          if parseDateTimeRegEx.test d1[0]
            #d1[0] is date with time data
            d1[0] = parseDateTime d1[0]
            isDateOnlyFormat = false
          else
            throw new Error 'Date/time format not recognized. Pick between \'YYYY-MM-DD\' or \'YYYY-MM-DD HH:MM:SS\'.'

      if !definedBlocks
        d1[2] = d3.timeSecond.offset d1[0], d.interval_s
      else
        if !(d1[2] instanceof Date)
          if parseDateRegEx.test d1[2]
            # d1[2] is date without time data
            d1[2] = parseDate d1[2]
          else
            if parseDateTimeRegEx.test d1[2]
              # d1[2] is date with time data
              d1[2] = parseDateTime d1[2]
            else
              throw new Error 'Date/time format not recognized. Pick between \'YYYY-MM-DD\' or \'YYYY-MM-DD HH:MM:SS\'.'
  return isDateOnlyFormat
