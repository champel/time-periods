export default cluster = (dataset, definedBlocks) ->
  # cluster data by dates to form time blocks
  dataset.forEach (series, seriesI) ->
    tmpData = [];
    dataLength = series.data.length;
    series.data.forEach (d, i) ->
      if (i isnt 0) and (i < dataLength)
        if d[1] is tmpData[tmpData.length - 1][1]
          # the value has not changed since the last date
          if (definedBlocks)
            if tmpData[tmpData.length - 1][2].getTime() is d[0].getTime()
              # end of old and start of new block are the same
              tmpData[tmpData.length - 1][2] = d[2]
              tmpData[tmpData.length - 1][3] = 1
            else
              tmpData.push(d)
          else
            tmpData[tmpData.length - 1][2] = d[2]
            tmpData[tmpData.length - 1][3] = 1
        else
          # the value has changed since the last date
          d[3] = 0
          if (!definedBlocks)
            # extend last block until new block starts
            tmpData[tmpData.length - 1][2] = d[0]
          tmpData.push(d);
      else
        if i is 0
          d[3] = 0;
          tmpData.push(d)
    dataset[seriesI].disp_data = tmpData
