import { JSDOM } from 'jsdom'
import * as d3 from 'd3'
import { expect } from 'chai'
import title from './title'

startDateTime = new Date "2016-01-01T11:00:00.000Z"
endDateTime = new Date "2016-01-01T12:00:00.000Z"
dataset = { foo: 'bar' }

describe 'title', () ->
    it 'should show title and subtitle', () ->
        body = new JSDOM().window.document.body
        titleProvider = (start, end, ds) ->
            expect(start).to.equal startDateTime
            expect(end).to.equal endDateTime
            expect(ds).to.deep.equal dataset
            'title'
        subtitleProvider = (start, end, ds) ->
            expect(start).to.equal startDateTime
            expect(end).to.equal endDateTime
            expect(ds).to.deep.equal dataset
            'subtitle'
        title d3.select(body), titleProvider, subtitleProvider, startDateTime, endDateTime, dataset
        expect(body).to.matchSnapshot()