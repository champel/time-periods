import * as d3 from "d3"

export default timeBlocks = (selection, yScale, xScale, customCategories, tooltipSupplier, div, dataset) ->
  longDateFormat = new Intl.DateTimeFormat('en-GB')

  # make y groups for different data series
  g = selection.selectAll('.g_data')
      .data dataset
      .enter()
      .append 'g'
      .attr 'transform', (d, i) -> "translate(0,#{yScale i})"
      .attr 'class', 'g_data dataset'

  # add data series
  g.selectAll 'rect'
      .data (d) -> d.disp_data.map (data) -> { categories: d.categories, data: data }
      .enter()
      .append('rect')
      .attr 'x', ({data}) -> xScale data[0]
      .attr 'y', 0
      .attr 'width', ({data}) -> xScale(data[2]) - xScale(data[0])
      .attr 'height', yScale.bandwidth()
      .attr 'class', ({data, categories}) ->
        category = data[1]
        if customCategories
            d3.select(this)
              .attr 'fill', categories[category].color
            return ''
        else
          if category is 1
            # data available
            return 'rect_has_data'
          else
            # no data available
            return 'rect_has_no_data'
      .on 'mouseover', ({data, categories}, i) ->
        [startDate, category, endDate] = data
        tooltip = customCategories and tooltip = categories[category].tooltip
        tooltip ?= tooltipSupplier and tooltip = tooltipSupplier startDate, category, endDate, i, dataset
        if tooltip
          matrix = @getScreenCTM().translate(+@getAttribute('x'), +@getAttribute('y'))
          div.transition()
            .duration 200
            .style 'opacity', 0.9
          div.html () -> tooltip
            .style 'left', () -> window.pageXOffset + matrix.e + 'px'
            .style 'top', () -> window.pageYOffset + matrix.f - 11 + 'px'
            .style 'height', yScale.bandwidth() + 11 + 'px'
      .on 'mouseout', () ->
        div.transition()
          .duration(500)
          .style 'opacity', 0
