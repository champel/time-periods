export default labels = (selection, width, yScale, dataset) ->
  # create y axis labels
  # text labels
  y_labels = selection.selectAll()
    .data dataset
    .enter()
  y_labels
    .filter (d) -> d.measure and not d.measure_html
    .append 'text'
    .attr 'x', 0
    .attr 'y', (d, i) -> (yScale i) + yScale.bandwidth()/2
    .text (d) -> d.measure
    .attr 'class', (d) -> returnCSSClass = "ytitle #{if d.measure_url then 'link'}"
    .on 'click', (d) ->
      if d.measure_url
        return window.open(d.measure_url)
      return null
  # HTML labels
  y_labels
    .data (d) -> d.map (datum, index) -> { datum, index}
    .filter (d) -> d.datum.measure_html
    .append 'foreignObject'
    .attr 'x', 0
    .attr 'y', (d, i) -> yScale d.index
    .attr 'width', width
    .attr 'height', yScale.bandwidth()
    .attr 'class', 'ytitle'
    .html (d) -> d.datum.measure_html
