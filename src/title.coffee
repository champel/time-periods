export default title = (selection, titleSupplier, subTitleSupplier, startDate, endDate, dataset) ->
  if titleSupplier and title = titleSupplier startDate, endDate, dataset
    selection
        .append 'text'
        .attr 'x', 0
        .attr 'y', 0
        .text title
        .attr 'class', 'heading'

  if subTitleSupplier and subTitle = subTitleSupplier startDate, endDate, dataset
    selection
        .append 'text'
        .attr 'x', 0
        .attr 'y', 17
        .text subTitle
        .attr 'class', 'subheading'
