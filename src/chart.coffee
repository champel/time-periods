import * as d3 from "d3"
import { paginate, hasDefinedBlocks, hasCustomCategories, normalize, cluster, range } from './dataset'
import grid from './grid'
import labels from './labels'
import timeBlocks from './timeBlocks'
import title from './title'
import legend from './legend'
import timeAxis from './timeAxis'

export default chart = () ->
  labelsWidth = 100        # width of the labels
  width = 600              # width of the labels

  dataHeight = 18          # height of horizontal data bars
  lineSpacing = 14         # spacing between horizontal data bars

  paddingTopHeading = -50  # vertical space for heading
  paddingBottom = 10       # vertical overhang of vertical grid lines on bottom

  tooltipSupplier = undefined
  titleSupplier = undefined
  subTitleSupplier = undefined
  emphasizeYearTicks = 1   # year ticks to be emphasized or not (default: yes)

  # define chart pagination
  maxDisplayDatasets = 0   # max. no. of datasets that is displayed, 0: all (default: all)

  # dataset that is displayed first in the current
  # display, chart will show datasets "curDisplayFirstDataset" to
  # "curDisplayFirstDataset+maxDisplayDatasets"
  curDisplayFirstDataset = 0

  # range of dates that will be shown
  # if from-date (1st element) or to-date (2nd element) is zero,
  # it will be determined according to your data (default: automatically)
  displayDateRange = [0, 0]

  # global div for tooltip
  div = d3.select('body').append('div')
      .attr 'class', 'tooltip'
      .style 'opacity', 0

  definedBlocks = null
  customCategories = null

  chart = (selection) ->
    margin =
      top: if titleSupplier then 70 else 20,
      right: 40                   # right margin should provide space for last horz. axis title
      bottom: 20
      left: labelsWidth           # left margin should provide space for y axis Labels

    dataWidth = width - margin.left - margin.right
    paddingLeft = -labelsWidth    # space for y axis Labels


    selection.each (dataset) ->
      [ startSet, endSet, maxPages ] = paginate dataset, maxDisplayDatasets, curDisplayFirstDataset
      selection.attr 'data-max-pages', maxPages # append data attribute in HTML for pagination interface

      definedBlocks = if definedBlocks is null then hasDefinedBlocks dataset
      customCategories = if customCategories is null then hasCustomCategories dataset

      normalize dataset, definedBlocks
      cluster dataset, definedBlocks
      [ startDate, endDate ] = range dataset, displayDateRange

      # define scales
      xScale = d3.scaleTime()
        .domain [startDate, endDate]
        .range [0, dataWidth]
        .clamp 1

      datasetSlice = dataset.slice startSet, endSet
      yScale = d3.scaleBand()
        .domain Array.from(Array(datasetSlice.length).keys())
        .range [0, (dataHeight + lineSpacing) * datasetSlice.length + lineSpacing]
        .paddingInner lineSpacing / (dataHeight + lineSpacing)
        .paddingOuter lineSpacing / (dataHeight + lineSpacing)

      # create SVG element
      svg = d3.select this
        .append 'svg'
        .attr 'width', width
        .attr 'height', (dataHeight + lineSpacing) * datasetSlice.length - 1 + margin.top + margin.bottom
        .append 'g'
        .attr 'transform', 'translate(' + margin.left + ',' + margin.top + ')'

      # create basic element groups
      if titleSupplier
        svg.append 'g'
          .attr 'id', 'g_title'
          .attr 'transform', "translate(#{paddingLeft}, #{paddingTopHeading})"
          .call title, titleSupplier, subTitleSupplier, startDate, endDate, datasetSlice

      if !customCategories
        svg.append 'g'
          .attr 'id', 'g_legend'
          .attr 'transform', "translate(#{dataWidth + margin.right - 150}, #{paddingTopHeading - 19})"
          .call legend

      if datasetSlice.length
        svg.append 'g'
          .attr 'id', 'x_axis'
          .call timeAxis, xScale, emphasizeYearTicks

        svg.append 'g'
          .attr 'id', 'g_axis'
          .call grid, yScale, xScale, datasetSlice

      svg.append 'g'
        .attr 'id', 'g_data'
        .call timeBlocks, yScale, xScale, customCategories, tooltipSupplier, div, datasetSlice

      svg.append 'g'
        .attr 'id', 'y_axis'
        .attr 'transform', "translate(#{paddingLeft},0)"
        .call labels, -paddingLeft, yScale, datasetSlice

  chart.labelsWidth = (_) ->
    if not arguments.length then return labelsWidth
    labelsWidth = _
    return chart

  chart.width = (_) ->
    if not arguments.length then return width
    width = _
    return chart

  chart.tooltipSupplier = (_) ->
    if not arguments.length then return tooltipSupplier
    tooltipSupplier = _
    return chart

  chart.titleSupplier = (_) ->
    if not arguments.length then return titleSupplier
    titleSupplier = _
    return chart

  chart.subTitleSupplier = (_) ->
    if not arguments.length then return subTitleSupplier
    subTitleSupplier = _
    return chart

  chart.maxDisplayDatasets = (_) ->
    if not arguments.length then return maxDisplayDatasets
    maxDisplayDatasets = _
    return chart

  chart.curDisplayFirstDataset = (_) ->
    if not arguments.length then return curDisplayFirstDataset
    curDisplayFirstDataset = _
    return chart

  chart.displayDateRange = (_) ->
    if not arguments.length then return displayDateRange
    displayDateRange = _
    return chart

  chart.emphasizeYearTicks = (_) ->
    if not arguments.length then return emphasizeYearTicks
    emphasizeYearTicks = _
    return chart

  chart
